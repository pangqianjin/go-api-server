package service

import (
	"fmt"
	"regexp"

	"gitee.com/go-apiServer/conf"
	"gitee.com/go-apiServer/libs"
	"gitee.com/go-apiServer/logger"
	"gitee.com/go-apiServer/model"
	"github.com/kataras/iris/v12"
)

type service struct {
	Property model.Property
}

func NewService() *service {
	property := model.Property{}
	libs.ReadFileToModel(conf.PropertyFileName, &property)

	fmt.Printf("%#v\n", property)
	return &service{
		Property: property,
	}
}

func (s *service) handler(ctx iris.Context) {
	url := ctx.Request().URL
	method := ctx.Method()
	path := fmt.Sprintf("%s/%s", s.Property.DataPath, url.Path)
	jsonReg := regexp.MustCompile(`.json`)
	suffixMap := map[string]string{
		"POST":   ".post.json",
		"DELETE": ".delete.json",
		"PUT":    ".put.json",
		"PATCH":  ".patch.json",
	}

	// replace "" to ".json" automatically if possible
	if !jsonReg.MatchString(path) {
		path += ".json"
	}

	if suffix, ok := suffixMap[method]; ok {
		path = jsonReg.ReplaceAllString(path, suffix) // replace .json to .xxx.json
	}

	contentByte, err := libs.ReadJSON(path)
	if err != nil {
		fmt.Println("err:", err)
		ctx.NotFound()
		return
	}
	ctx.Write([]byte(contentByte))
}

func (s *service) newApp() *iris.Application {
	app := iris.New()

	// handle statics resources
	statics := app.Party("/")
	statics.HandleDir(s.Property.PublicPrefix, iris.Dir(s.Property.PublicPath), iris.DirOptions{
		Compress:   false,
		ShowList:   false,
		ShowHidden: false,
		Cache: iris.DirCacheOptions{
			// enable in-memory cache and pre-compress the files.
			Enable: s.Property.MemoryCache || false,
			// do not compress files smaller than size.
			CompressMinSize: 300,
			// available encodings that will be negotiated with client's needs.
			Encodings: []string{"gzip", "br" /* you can also add: deflate, snappy */},
		},
		DirList: iris.DirListRich(),
	})

	return app
}

func (s *service) AddRoutes(app *iris.Application) {
	app.Any("/{request:string}", s.handler)
	app.Any("/{group:string}/{request:string}", s.handler)
	app.Any("/{prefix:string}/{group:string}/{request:string}", s.handler)

	return
}

func (s *service) Start() {
	logger := logger.CustomLogger()
	app := s.newApp()
	app.Use(logger)
	s.AddRoutes(app)
	app.Logger().SetLevel(s.Property.DebugLevel)
	app.Listen(fmt.Sprintf(":%s", s.Property.Port))
}
